!function($) {
let btnTextAceptar = 'ACEPTAR TODAS LAS COOKIES';
const btnMySelection = $('<a id="CookiebotNuevoBtnGuardar" ' + 'class="CybotCookiebotDialogBodyButton btnSantaCookieBot" ' + 'href="javascript: void(0);" tabindex="0" lang="es" ' + 'style="width: 177px; display: block;">Guardar</a>');
const btnText = $('#CybotCookiebotDialogBodyButtonAccept').text();
const btnMyCookies = $('<a id="CookiebotNuevoBtnAllSelect" ' + 'class="CybotCookiebotDialogBodyButton btnSantaCookieBot" ' + 'href="javascript: void(0);" tabindex="0" lang="es" ' + 'style="width: 200px; display: block;"></a>');
btnMyCookies.on('click', function() {
  Cookiebot.submitCustomConsent(1, 1, 1);
  $('#CybotCookiebotDialog').delay(200).fadeOut();
  closeOverlay()
});
btnMySelection.on('click', function() {
  const a = $("#CybotCookiebotDialogBodyLevelButtonPreferences").prop("checked")
    , b = $("#CybotCookiebotDialogBodyLevelButtonStatistics").prop("checked")
    , c = $("#CybotCookiebotDialogBodyLevelButtonMarketing").prop("checked");
  Cookiebot.submitCustomConsent(a, b, c);
  console.log(a + ' - ' + b + ' - ' + c);
  $('#CybotCookiebotDialog').delay(50).fadeOut();
  closeOverlay()
});


function addBtnsCookiebot(btnTextAceptar) {
  $('#CybotCookiebotDialogBodyButtons').append(btnMyCookies);
  btnMyCookies.text(btnTextAceptar);
  $('#CybotCookiebotDialog').addClass('showing');
  $('#CybotCookiebotDialogDetailBodyContentCookieContainerTypes').append(btnMySelection)
}
function closeOverlay() {
  $('#CybotCookiebotDialogBodyUnderlay').remove();
  $('body').css('overflow', 'initial')
}

function addBtnsCookiebot() {
  $('#CybotCookiebotDialogBodyButtons').append(btnMyCookies);
  btnMyCookies.text(btnTextAceptar);
  $('#CybotCookiebotDialog').addClass('showing');
  $('#CybotCookiebotDialogDetailBodyContentCookieContainerTypes').append(btnMySelection)
}
function closeOverlay() {
  $('#CybotCookiebotDialogBodyUnderlay').remove();
  $('body').css('overflow', 'initial')
}

window.addEventListener('CookiebotOnDialogDisplay', function() {
  const btnTextAceptar = $('#CybotCookiebotDialogBodyButtonAccept').text();
  addBtnsCookiebot(btnTextAceptar)
}, !1);

addBtnsCookiebot(btnText);
}(jQuery);
