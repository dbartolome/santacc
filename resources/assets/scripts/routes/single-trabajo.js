import 'lightgallery/dist/js/lightgallery';
import 'lightgallery/modules/lg-fullscreen';
import 'lightgallery/modules/lg-hash';
import 'lightgallery/modules/lg-thumbnail';
import 'lightgallery/modules/lg-video';
import Player from '@vimeo/player';

export default {
  init() {
    // JavaScript to be fired on the about us page



    let lg = $('#relative-caption');
    lg.lightGallery({
      selector: '.linkENlace',
      mode: 'lg-fade',
      cssEasing : 'cubic-bezier(0.25, 0, 0.25, 1)',
      thumbnail: true,
      thumbWidth: 200,
      thumbheight: 90,
      subHtmlSelectorRelative: true,
      loadYoutubeThumbnail: true,
      youtubeThumbSize: 'default',
      loadVimeoThumbnail: true,
      vimeoThumbSize: 'thumbnail_medium',
      toogleThumb: true,
      hash: true,
    });


    var iframe = document.querySelectorAll('iframe');

    let player = [];

    for (let i=0; i<iframe.length; i++) {
      player[i] = new Player(iframe[i]);
    }

    let todosRproductores = document.querySelectorAll('.videoCard');
   // console.log(todosRproductores);


    for (let i=0; i<todosRproductores.length; i++){
      todosRproductores[i].addEventListener('mouseover',function(){
        $(this).find('.playImg').css('opacity', 0);
        player[i].setVolume(1);
        player[i].play();
      });
      todosRproductores[i].addEventListener('mouseout',function(){
        $(this).find('.playImg').css('opacity', 1);
        player[i].pause();
      });
    }

    $(window).on('scroll', function () {
      let tamanoBody = $('body').height();
      let tamanoWindow = $(window).height();

      if (($(window).width() > 992) && (tamanoBody > tamanoWindow)) {
        if ($(window).scrollTop() > 170) {
          $('.banner').addClass('headerSticky');
          $('.main').css('margin-top', '14%');
        } else {
          $('.banner').removeClass('headerSticky');
          $('.main').css('margin-top', 0);
        }
      }
    });

    $(window).on('resize', function () {
      let tamanoBody = $('body').height();
      let tamanoWindow = $(window).height();
      if (($(window).width() > 992) && (tamanoBody > tamanoWindow)) {
        if ($(window).scrollTop() > 5) {
          $('.banner').addClass('headerSticky');
        } else {
          $('.banner').removeClass('headerSticky');
        }
      }
    });


   /* function playPause (item, index) {
      $('.videoCard').on('mouseover',function(){
        $(this).find('.playImg').css('opacity', 0);
        player.play();
      });

      $('.videoCard').on('mouseout',function(){
        $(this).find('.playImg').css('opacity', 1);
       // player[index].pause();
      });
      console.log(item, index);
     // document.getElementById("demo").innerHTML += index + ":" + item + "<br>";
    } */



    /*
        $('#html5-videos').lightGallery({
          selector: 'div',
          mode: 'lg-fade',
        });

        $('#video-gallery').lightGallery({
          loadYoutubeThumbnail: true,
          youtubeThumbSize: 'default',
          loadVimeoThumbnail: true,
          vimeoThumbSize: 'thumbnail_medium',
        }); */
  },
};
