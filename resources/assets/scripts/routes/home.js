//const InfiniteScroll = require('infinite-scroll');
import { gsap } from 'gsap';
//import { ScrollTrigger } from 'gsap/ScrollTrigger';
//import { CSSRulePlugin } from 'gsap/CSSRulePlugin';
//import { TextPlugin } from 'gsap/TextPlugin';

export default {
  init() {


    /*registramos el plugin
   gsap.registerPlugin(ScrollTrigger, CSSRulePlugin, TextPlugin);

    //paso a un array el numero de items qeu se repiten con la clase
    const boxes = gsap.utils.toArray('.itemTrabajo');


     boxes.forEach((box) => {
       const anim = gsap.fromTo(box, {autoAlpha: 0, y: 25, filter: blur(100)}, {duration: 0.7, autoAlpha: 1, y: 0, filter: 'none'});
       ScrollTrigger.create({
         trigger: box,
         animation: anim,
         toggleActions: 'restart none none none',
         once: true,
         duration: 1,
       });
     });


    gsap.from('.btnSeleccion', {duration: 2, text: ''})

*/

    /*
       gsap.to('.itemTrabajo',{
           scrollTrigger: {
             trigger: '.itemTrabajo',
             toggleActions: 'restart none none none',
           },
           rotation: 2,
           duration: 2,
         });



        function hide(elem) {
          gsap.set(elem, {autoAlpha: 0});
        }

        function animateFrom(elem, direction) {
          direction = direction | 1;
          var dirX;
          var dirY;
          dirY = direction * 100;
          console.log(dirY);
          console.log(dirX);
          if(elem.classList.contains('itemTrabajo')) {
            dirX = 0;
            dirY = -20;
          }
          gsap.fromTo(elem, {x: dirX, y: dirY, autoAlpha: 0, filter: blur(100)}, {
            duration: 1.25,
            x: 0,
            y: 0,
            autoAlpha: 1,
            filter: 'none',
            ease: 'expo',
            overwrite: 'auto',
          });
        }


          gsap.utils.toArray('.itemTrabajo').forEach(function(elem) {
            hide(elem); // assure that the element is hidden when scrolled into view

            ScrollTrigger.create({
              trigger: elem,
              onEnter: function() { animateFrom(elem) },
              onEnterBack: function() { animateFrom(elem, -1) },
              onLeave: function() { hide(elem) }, // assure that the element is hidden when scrolled into view
              duration: 2,
            });
          });
    */
    /* $(window).on('scroll', function () {
      let tamanoBody = $('body').height();
      let tamanoWindow = $(window).height();
      if (($(window).width() > 992) && (tamanoBody > tamanoWindow)) {
        if ($(window).scrollTop() > 170) {
          $('.banner').addClass('headerSticky');
          $('.main').css('margin-top', '14%');
        } else {
          $('.banner').removeClass('headerSticky');
          $('.main').css('margin-top', 0);
        }
      }
    });

    $(window).on('resize', function () {
      let tamanoBody = $('body').height();
      let tamanoWindow = $(window).height();
      if (($(window).width() > 992) && (tamanoBody > tamanoWindow)) {
        if ($(window).scrollTop() > 5) {
          $('.banner').addClass('headerSticky');
        } else {
          $('.banner').removeClass('headerSticky');
        }
      }
    });


     */

    let varFondoGif = '';


    $('.itemTrabajo').on('mouseenter',function (e) {
      e.preventDefault();
      varFondoGif = $('.imagenItemTrabajo',this).attr('data-fondoGif');
      let capaImagen = $('.imagenItemTrabajo',this);
      let capaFondo = $('.rollOverTrabajo',this);
      let capaTrabajo =  $('.capaTrabajo',this);
      let imgGif = `<img src="${varFondoGif}" width="100%" class="rollOverTrabajo" alt="SantaCC Compañia Creativa ">`;
      $(this).append(imgGif);
      gsap.fromTo(capaTrabajo, {autoAlpha: 0}, {duration: 0.5, autoAlpha: 1});
      gsap.fromTo(capaImagen, {autoAlpha: 1}, {duration: 0.5, autoAlpha: 0});
      gsap.fromTo(capaFondo, {autoAlpha: 1}, {duration: 0.5, autoAlpha: 0});
    });

    $('.itemTrabajo').mouseleave(function (e) {
      e.preventDefault();
      let capaImagen = $('.imagenItemTrabajo',this);
      let capaFondo = $('.rollOverTrabajo',this);
      let capaTrabajo =  $('.capaTrabajo',this);
      gsap.fromTo(capaTrabajo, {autoAlpha: 1}, {duration: 0.5, autoAlpha: 0});
      gsap.fromTo(capaImagen, {autoAlpha: 0}, {duration: 0.5, autoAlpha: 1});
      gsap.fromTo(capaFondo, {autoAlpha: 0}, {duration: 0.5, autoAlpha: 1});

    })

   /* function quitarCapa() {
      $('.rollOverTrabajo').remove();
    }*/

    let colorSantaCreative = $('.santa-creative').attr('data-color');
    let colorSantaForBussines = $('.santa-for-bussines').attr('data-color');


    $('#santa-creative').on('click',function(){
      $('.etiquetaTipo').html('creative');
      $('.itemTrabajo').css({
          'display':'none',
          'background-color': colorSantaCreative,
        })
      $('.banner').css({
        'background-color': colorSantaCreative,
      });
      $('footer').css({
        'background-color': colorSantaCreative,
      });
      $('.banner nav ul li').css({
        'border-bottom': '2px solid '+colorSantaCreative,
      });
      $('.santa-creative').css({
          'display': 'block',
          'opacity':'1',
          'visibility':'initial',
        });
    });

    $('#santa-for-bussines').on('click',function(){
      $('.etiquetaTipo').html('for bussines');
      $('.itemTrabajo').css({
        'display':'none',
        'background-color': colorSantaForBussines,
      });
      $('.banner').css({
        'background-color': colorSantaForBussines,
      });
      $('footer').css({
        'background-color': colorSantaForBussines,
      });
      $('.banner nav ul li').css({
        'border-bottom': '2px solid '+colorSantaForBussines,
      });
      $('.santa-for-bussines').css({
        'display':'block',
        'opacity':'1',
        'visibility':'initial',
      });
    });
    $('#ver-todos').on('click',function(){
      $('.etiquetaTipo').html('');
      $('.itemTrabajo').css({
        'display':'block',
        'background-color': '#d6ce00',
      });
      $('.banner').css({
        'background-color': '#d6ce00',
      });
      $('footer').css({
        'background-color': '#d6ce00',
      });
      $('.banner nav ul li').css({
        'border-bottom': '2px solid #d6ce00',
      });
    });
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
