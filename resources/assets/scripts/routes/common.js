//import { gsap } from 'gsap';
//import { CSSRulePlugin } from 'gsap/CSSRulePlugin';

export default {
    init() {
        // JavaScript to be fired on all pages

        $('.hamburger').click(function () {
            $(this).toggleClass('is-active');
            $('.menuMovil').toggleClass('verMovil');
            $('.wrap').toggleClass('moverWrap');

        });



/*
      gsap.registerPlugin(CSSRulePlugin);
      gsap.fromTo(
        '.wrap',
        {autoAlpha: 0},{duration: 1, autoAlpha: 1});
*/


    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};
