import { gsap } from 'gsap';
import { CSSRulePlugin } from 'gsap/CSSRulePlugin';

export default {
  init() {
    // JavaScript to be fired on the about us page
    var ventana_ancho = $(window).width();
    //var ventana_alto = $(window).height();

    console.log(ventana_ancho);

    gsap.registerPlugin(CSSRulePlugin);

    if(ventana_ancho >= 768) {
      $('.contEquipo').on('mouseenter',function (e) {
        e.preventDefault();
        let capaTxt = $('.capaTxtTranparente',this);
        gsap.fromTo(capaTxt , {autoAlpha: 0}, {duration: 0.5, autoAlpha: 1});

      });

      $('.contEquipo').on('mouseleave',function (e) {
        e.preventDefault();
        let capaTxt = $('.capaTxtTranparente',this);
        gsap.fromTo(capaTxt, {autoAlpha: 1}, {duration: 0.5, autoAlpha: 0});
      });
    } else {
      $('.contEquipo').on('click',function (e) {
        e.preventDefault();
        let capaTxt = $('.capaTxtTranparente',this);
        gsap.fromTo(capaTxt , {autoAlpha: 0}, {duration: 0.5, autoAlpha: 1});

        setTimeout(function(){ gsap.fromTo(capaTxt, {autoAlpha: 1}, {duration: 0.5, autoAlpha: 0}); }, 3000);
      });


    }

    /* $( window ).resize(function() {
      var ventana_ancho = $(window).width();
      if(ventana_ancho >= 768) {
        $('.contEquipo').on('mouseenter',function (e) {
          e.preventDefault();
          let capaTxt = $('.capaTxtTranparente',this);
          gsap.fromTo(capaTxt , {autoAlpha: 0}, {duration: 0.5, autoAlpha: 1});
        });

        $('.contEquipo').on('mouseleave',function (e) {
          e.preventDefault();
          let capaTxt = $('.capaTxtTranparente',this);
          gsap.fromTo(capaTxt, {autoAlpha: 1}, {duration: 0.5, autoAlpha: 0});
        });
      } else {
        $('.contEquipo').on('click',function (e) {
          e.preventDefault();
          let capaTxt = $('.capaTxtTranparente',this);
          gsap.fromTo(capaTxt , {autoAlpha: 0}, {duration: 0.5, autoAlpha: 1});
          setTimeout(function(){ gsap.fromTo(capaTxt, {autoAlpha: 1}, {duration: 0.5, autoAlpha: 0}); }, 3000);
        });


      }
    }); */




  },
};
