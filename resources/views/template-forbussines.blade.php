{{--
  Template Name: Plantilla grid For Bussines
--}}

@extends('layouts.app')
@section('content')
  <div class="container-fluid">
  </div>
  <section style="width: 100%; height: 80vh; display: flex; align-content: center; align-items: center; align-self: center; justify-content: center; position: relative; overflow: hidden">
    <div class="fondoHeadHome text-center">

      <video preload="true" autoplay="autoplay" loop volume="0" class="fillWidth">
        <source src="/wp-content/themes/santacc/dist/images/home_bussines.mp4">
      </video>


      <p>
        MENSAJES CLAROS. UNA IMAGEN QUE EMOCIONA. LA PALABRA PRECISA. IDEAS NÍTIDAS QUE LLEGAN Y QUE MUEVEN A LA ACCIÓN.

      </p>
    </div>

  </section>
  <section>
    @php


      $args = array(
            'post_type'=>'trabajo',
            'posts_per_page'=>'-1',
            'tax_query' => array(
              array(
                  'taxonomy' => 'tipotrabajo',
                  'field' => 'slug',
                  'terms' => 'santa-for-bussines',
              ),
            ),
      );
      $loop = new WP_Query($args);


    @endphp
    <div class="container-fluid">
      <div class="row">

        <?php $contenedorTrabajos = '';$i = 0; ?>
        @while ($loop->have_posts()) @php $loop->the_post() @endphp

  {{-- @include('partials.content-forbusiness') --}}

      <?php
        $logoCampana = get_field('logoCliente');
        $contLogo = '';
        $textoCampana = get_field('campania');
        $titulo = get_the_title();
        $urlImagen = get_the_post_thumbnail_url();
        $enlaceEntrada = get_the_permalink();
        if($logoCampana){
          $contLogo .= '<img src="'. $logoCampana["url"] .'" class="imagenLogo" alt="'. $logoCampana["title"].'">';
        }

        if($i <= 3) {
          $contenedorTrabajos .= '<a href="'.$enlaceEntrada.'" class="col-12 col-md-6 itemTrabajo" >
          <img src="'.$urlImagen.'" width="100%" class="imagenItemTrabajo" alt="SantaCC - {!! get_the_title() !!}">
          <div class="capaTrabajo">
                <h2>'.$titulo.'</h2>
                <div class="descTrabajo">'.$textoCampana.'</div>
          </div>
        </a>';
          $i++;
        } else if( $i == 4 ){
          $contenedorTrabajos .= '<a href="'.$enlaceEntrada.'" class="col-12 col-md-12 itemTrabajo" >
          <img src="'.$urlImagen.'" width="100%" class="imagenItemTrabajo" alt="SantaCC - '.$titulo.'">
          <div class="capaTrabajoGrande">
                <h2>'.$titulo.'</h2>
                <div class="descTrabajo">'.$textoCampana.'</div>
          </div>
        </a>';
          $i++;
        } else if( $i >= 5 ){
          $contenedorTrabajos .= '<a href="'.$enlaceEntrada.'" class="col-12 col-md-12 itemTrabajo" >
          <img src="'.$urlImagen.'" width="100%" class="imagenItemTrabajo" alt="SantaCC - '.$titulo.'">
          <div class="capaTrabajoMedio">
                <h2>'.$titulo.'</h2>
                <div class="descTrabajo">'.$textoCampana.'</div>
          </div>
        </a>';
          $i++;
        }


        ?>


          <?php ?>
        @endwhile


        <?php echo $contenedorTrabajos; ?>
      </div>
</div>


</section>


@endsection

<style>

.fondoHeadHome>p  { font-size: 2.5rem; color:black; padding-top: 10px;max-width: 1400px; margin: 0 auto;}
.fondoHeadHome    { /*background-image: url("banner-1920x400-1.jpg");*/ background-position: center;  background-size: 1920px 400px; width: 100%;}


video {
position: absolute;
top: 50%;
left: 50%;
height: inherit;
z-index: -1;
transform: translateX(-50%) translateY(-50%);
transition: 1s opacity;
width: 100%;
}
</style>
