{{--
  Template Name: Plantilla grid Creative
--}}

@extends('layouts.app')
@section('content')
  <section>
    @php


      $args = array(
            'post_type'=>'trabajo',
            'posts_per_page'=>'-1',
            'tax_query' => array(
              array(
                  'taxonomy' => 'tipotrabajo',
                  'field' => 'slug',
                  'terms' => 'santa-creative',
              ),
            ),
      );
      $loop = new WP_Query($args);

    @endphp
    <div class="container especialHome">
      <div class="row">
        @while ($loop->have_posts()) @php $loop->the_post() @endphp
        @include('partials.content-'.get_post_type())
        @endwhile
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-9">
        <div style="text-align: center; border-radius: 50px; padding: 5%; background-color: #fff; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); font-size: 1.1rem">¿Quieres saber más? <a href="/contacto/" style="text-decoration: underline">Escríbenos</a> </div>
      </div>
    </div>
    </div>

  </section>


@endsection
