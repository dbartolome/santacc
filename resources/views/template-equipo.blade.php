{{--
  Template Name: Plantilla grid equipo
--}}

@extends('layouts.app')

@section('content')
  @include('partials.page-header')


  @php

      $args = array('post_type'=>'equipo','posts_per_page'=> -1,'orderby' => 'rand');
      $loop = new WP_Query($args);
  @endphp
  <div class="container">
    <div class="row g-0 justify-content-md-center">
      @while ($loop->have_posts()) @php $loop->the_post() @endphp
      @include('partials.content-'.get_post_type())
      @endwhile
        <div class="col-6 col-md-4 col-lg-3 contEquipo align-self-center">

          <img src="https://santacc.es/wp-content/uploads/2021/06/logo_santa_equipo.jpg" width="100%" class="imagenItemTrabajo" alt="SantaCC">
        </div>
    </div>
  </div>



@endsection
