{{--
  Template Name: Plantilla grid Trabajos
--}}

@extends('layouts.app')

@section('content')
  @include('partials.page-header')


  @php
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
      $args = array('post_type'=>'trabajo','posts_per_page'=>3,'paged' => $paged);
      $loop = new WP_Query($args);
  @endphp
  <div class="container">
    <div class="row justify-content-center row-eq-height">
      @while ($loop->have_posts()) @php $loop->the_post() @endphp
      @include('partials.content-'.get_post_type())
      @endwhile
    </div>
  </div>
  <?php
  // next_posts_link() usage with max_num_pages.
  next_posts_link( __( 'Older Entries', 'textdomain' ), $loop->max_num_pages );
  previous_posts_link( __( 'Newer Entries', 'textdomain' ) );

  // Clean up after the query and pagination.

  ?>

  {!! get_the_posts_navigation() !!}
@endsection
