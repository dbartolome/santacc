<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/js/all.min.js" integrity="sha512-cyAbuGborsD25bhT/uz++wPqrh5cqPh1ULJz4NSpN9ktWcA6Hnh9g+CWKeNx2R0fgQt+ybRXdabSBgYXkQTTmA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

@extends('layouts.app')
@section('content')
  <section>
    @php


        $args = array('post_type'=>'trabajo','posts_per_page'=>'8');
        $loop = new WP_Query($args);

    @endphp
    <div class="container-fluid" style="background-color: white; position: fixed; top: 0; left: 0; z-index: -1">
      <div class="row">
        <div class="col-12" style="display: flex; font-size: 4rem; height: 80vh; align-content: center; align-items: center; align-self: center; justify-content: center;">
          _THAT'S SANTA &#128519;
        </div>
      </div>
    </div>
    <div class="container-fluid" style="margin-top: 80vh;">
          <div class="row">
            @while ($loop->have_posts()) @php $loop->the_post() @endphp
            @include('partials.content-trabajohome')
            @endwhile
          </div>
      </div>
     <!-- <div class="row justify-content-center">
        <div class="col-9">
            <div style="text-align: center; border-radius: 50px; padding: 5%; background-color: #fff; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); font-size: 1.1rem">¿Quieres saber más? <a href="/contacto/" style="text-decoration: underline">Escríbenos</a> </div>
        </div>
      </div>
    </div> -->

  </section>
  <section>

    <?php



    $contServicios = '';

    $contServicios = '<div class="container"><div class="row">';
        // Loop over sub repeater rows.
        if( have_rows('servicios','options') ):
          while( have_rows('servicios','options') ) : the_row();

            // Get sub value.
            $iconoServicios = get_sub_field('iconoServicios');
            $codigoIconosServicio = get_sub_field('codigoIconosServicio');
            $tamanoIcono = get_sub_field('tamanoIcono');
            $tituloServicio = get_sub_field('tituloServicio');
            $nombreServicio = get_sub_field('nombreServicio');
            if($codigoIconosServicio != '') {
              $contServicios .= ' <div class="col-4 p-5"><div style="display: block; text-align: center; margin: 2% 0;"><i class="'. $codigoIconosServicio .' fa-'.$tamanoIcono.'x"></i> </div><div style="font-weight: bold; text-transform: uppercase; margin: 5% 0; font-size: 2rem">'.$tituloServicio.' </div><div style=""> '.$nombreServicio .'</div></div>';
            } else {
              $contServicios .= ' <div class="col-4 p-5"><img src="'. $iconoServicios["url"] .'"> <div style="font-weight: bold; text-transform: uppercase; margin: 5% 0; font-size: 2rem">'.$tituloServicio.' </div><div style=""> '.$nombreServicio .'</div></div>';
            }

          endwhile;
        endif;
        $contServicios .= '</div></div>';
    ?>

    <section style="background-color: white">
      <?php echo $contServicios; ?>
    </section>


@endsection


