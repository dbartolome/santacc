{{--
  Template Name: Template Agencia
--}}

<?php

  $imagenFondo = get_field('imagenFondo','option');
  $imagenSuperior = get_field('imagenSuperior','option');

  ?>

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  @include('partials.page-header')
  @include('partials.content-page')

    <div class="contManifiesto">
      <div class="imgManifiesto">
        <img src="<?php echo $imagenSuperior['url'];?>" width="100%" alt="<?php echo $imagenSuperior['title'];?>">
      </div>
      <img src="<?php echo $imagenFondo['url']; ?>" width="100%" alt="<?php echo $imagenFondo['title'];?>>

    </div>

  @endwhile
@endsection

