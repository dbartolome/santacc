<?php
/*$logoCampana = get_field('logoCliente');
$contLogo = '';
$urlFinalGif = '';
$urlGif = get_field('fondoAnimado');
if(!$urlGif) {
  $urlFinalGif = get_the_post_thumbnail_url();
}else {
  $urlFinalGif = $urlGif["url"];
}
if($logoCampana){
  $contLogo .= '<img src="'. $logoCampana["url"] .'" class="imagenLogo" alt="'. $logoCampana["title"].'">';
}*/

  $puestoEquipo = get_field('puesto');
  $imagenFiltro = get_field('imagenFiltro');
?>
<div class="col-6 col-md-4 col-lg-3 contEquipo align-self-center">
  <div class="capaTxtTranparente" style="background-image: url({!!$imagenFiltro["url"]!!});">
    <div style="position: relative; height: fit-content">
      <div class="nombreEquipo">{!! get_the_title() !!}</div>
      <div class="puestoEquipo"><?php echo $puestoEquipo; ?></div>
    </div>
  </div>
  <img src="<?php echo get_the_post_thumbnail_url(); ?>" width="100%" class="imagenItemTrabajo" alt="SantaCC - {!! get_the_title() !!}">
</div>
