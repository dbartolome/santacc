<?php
global $post;
$listadoTaxonomis = '';

$terms = get_the_terms( $post->ID,'tipotrabajo', '', ' ');

if($terms != '') {
  foreach ( $terms as $term ) {
    $pasoAminusculas = $term->slug;
    $listadoTaxonomis  .= $pasoAminusculas;

  }
}

$varMia = '';

if( have_rows('colorTrabajo', 'option') ):
  while( have_rows('colorTrabajo', 'option') ) : the_row();
    $sub_tipo = get_sub_field('tipoTrabajo');
    $sub_color = get_sub_field('colorTrabajo');
      if($sub_tipo->slug == $listadoTaxonomis) {
        $varMia = $sub_color;
      }
    endwhile;
  else :
endif;


$logoCampana = get_field('logoCliente');
$contLogo = '';
$urlFinalGif = '';
$urlGif = get_field('fondoAnimado');
$imagenHome = get_field('imagenHome');

if(!$urlGif) {
  $urlFinalGif = $imagenHome["url"];
}else {
  $urlFinalGif = $urlGif["url"];
}
if($logoCampana){
  $contLogo .= '<img src="'. $logoCampana["url"] .'" class="imagenLogo" alt="'. $logoCampana["title"].'">';
}
?>
<a href="{!! the_permalink() !!}" class="col-12 col-md-4 col-lg-3 itemTrabajo <?php echo $listadoTaxonomis; ?>" data-color="<?php echo $varMia; ?>">
  <div class="rollOver"></div>
   <!-- <h2>{!! get_the_title() !!}</h2> -->

    <img src="<?php echo $imagenHome["url"]; ?>" width="100%" class="imagenItemTrabajo" data-fondoGif="<?php echo $urlFinalGif ?>" alt="SantaCC - {!! get_the_title() !!}">

    <div class="capaTrabajo">
      <div class="capaTransparente"></div>
      <div class="textoTrabajo" >
        <div class="capaTexto">
          <h2>{!! get_the_title() !!}</h2>
          <hr/>
          <div class="descTrabajo">{!! get_field('campania') !!}</div>
        </div>
        <!-- <div class="logoCampana"><?php echo $contLogo; ?></div> -->
      </div>

    </div>


</a>

