<?php

$imgLogo = get_field('logoHeader', 'option');

$theTitle = get_the_title();
$classHeader = '';

if($theTitle == 'Contacto' || $theTitle == 'Agencia' ) {
  $classHeader = 'banner headerSticky';
} else {
  $classHeader = 'banner';
}

$host= $_SERVER["HTTP_HOST"];

$capaPruebas = '';
if($host == 'santacc.site') {
  $capaPruebas = '<div class="avisoPruebas">Ojo: estás en la página de pruebas. <br /> <a href="https://santacc.es" style="color: red;text-decoration: underline">Click aquí para ir a la web oficial santacc.es</a></div>';
  echo $capaPruebas;
}



?>

<header class="<?php echo $classHeader; ?>">
    <button class="hamburger hamburger--spin" type="button"><span class="hamburger-box"><span class="hamburger-inner"></span></span></button>
</header>

<div class="menuMovil">
  @if (has_nav_menu('primary_navigation'))
    {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
  @endif
</div>
