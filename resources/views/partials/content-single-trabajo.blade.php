
<div class="container">
  <div class="row row-eq-height">
   <!-- <div class="col-12 col-lg-6" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>); background-size: cover;">
        <img src="<?php echo get_the_post_thumbnail_url(); ?>" width="100%" class="align-middle">
    </div> -->
    <div class="col-12 col-lg-12 align-self-center pb-5">
      <article @php post_class() @endphp>
        <header>
          <h1 class="entry-title" style="text-align: center">
              {!! get_the_title() !!}
          </h1>
        </header>
        <div class="entry-content">
          @php the_content() @endphp
          <div style="text-align: center; font-size: 1.3rem; margin-bottom: 3%;">{!! get_field('campania') !!}  </div>
          <div style="">{!! get_field('descripcion') !!}</div>
        </div>
      </article>
    </div>
  </div>
</div>

<?php

$contDestacado = '';
if( have_rows('trDestacados') ):

  // Loop through rows.
  while( have_rows('trDestacados') ) : the_row();

    // Load sub field value.
      $imagenDestacado = get_sub_field('imageDestacada');
      $videoDesatcado = get_sub_field('idVideoDestacado');

    if($imagenDestacado != '') {
      $contDestacado .= '<div class="container"><div class="col-12 mb-4" style="padding: 0"><img src="'. $imagenDestacado['url'] .'" alt="'. $imagenDestacado['name'] .'" width="100%"></div></DIV>';
      $imagenDestacado = '';
    }
    if($videoDesatcado != ''){
      $contDestacado .= '<div class="container"><div class="ratio ratio-16x9 mb-4">
		                        <iframe src="//player.vimeo.com/video/'. $videoDesatcado .'?controls=1?background=1&loop=1" allowfullscreen></iframe>
	                          </div></div>';
      $videoDesatcado = '';
          }

  endwhile;

// No value.
else :
  // Do something...
endif;
?>


<?php echo $contDestacado; ?>

<?php
// Check rows exists.



  $contenedorClipping = '';


if( have_rows('clipping') ):

  // Loop through rows.
  while( have_rows('clipping') ) : the_row();

    // Load sub field value.
    $nombreMedio = get_sub_field('nombreMedio');
    $titularNoticia = get_sub_field('titularNoticia');
    $urlNoticia = get_sub_field('urlNoticia');

      $contenedorClipping .= '<a href="'. $urlNoticia .'" target="_balnk" class="conjuntoLink"><h3 class="titClipping">'. $nombreMedio .'</h3>';
      $contenedorClipping .= '<div class="descClipping">'. $titularNoticia .'</div></a>';

  endwhile;

// No value.
else :
  // Do something...
endif;
$selectorClipping = get_field('tieneClipping');

?>


  <?php
    // Check rows exists.
        $contenedorMostrar = '';
        $contenedorMultiples = '';
        $rows = get_field('galeriaImagenes' );




  if( have_rows('galeriaImagenes') ):
      //$contenedorMostrar .= '<h2 style="text-align: center; margin: 2% 0;">Gráficas</h2><div class="container-fluid"><div class="row justify-content-center grid" id="relative-caption">';




        // Loop through rows.
         $j = 0;
         $i = 0;
         $k = 0;
        while( have_rows('galeriaImagenes') ) : the_row();
          $contenedorMultiples = '';
            // Load sub field value.
          $imagenGaleria = get_sub_field('imageGaleria');
          $txtGaleria = get_sub_field('txtImagenes');
          $descGaleria = get_sub_field('descImagenes');
          $urlVideo = get_sub_field('url_Video');
          $urlWeb = get_sub_field('url_web');
          $idVideo = get_sub_field('idVideo');



          $images = get_sub_field('grupoImagenes');
          $indicadores = '';
          $indicadoresFinal = '';
          $slidesimagenes = '';


          if( $images ){
            $i=0;
            foreach( $images as $image ) {
              if($i == 0) {
              $indicadores .= '<li data-bs-target="#carouselExampleIndicators'.$j.'" data-bs-slide-to="'.$i.'" class="active"></li>';
              $slidesimagenes .= '<div class="carousel-item active"><a href="'. esc_url($image['url']) .'" class="videoVer linkENlace"><img src="'. esc_url($image['url']) .'" alt="'. esc_attr($image['alt']) .'" width="100%" class="d-block w-100"/></a></div>';
              $i++;
              } else {
                $indicadores .= '<li data-bs-target="#carouselExampleIndicators'.$j.'" data-bs-slide-to="'.$i.'"></li>';
                $slidesimagenes .= '<div class="carousel-item"><a href="'. esc_url($image['url']) .'" class="videoVer linkENlace"><img src="'. esc_url($image['url']) .'" alt="'. esc_attr($image['alt']) .'" width="100%" class="d-block w-100"/></a></div>';
                $i++;
              }
            }
            $contenedorMultiples .= '<div id="carouselExampleIndicators'.$j.'" class="carousel slide" data-bs-ride="carousel"> <div class="carousel-inner">';
            $indicadoresFinal .= '<ol class="carousel-indicators">'.$indicadores.'</ol>';
            $contenedorMultiples .= $slidesimagenes;
            $contenedorMultiples .= '</div>'.$indicadoresFinal.'</div>';
            $j++;
            $i++;
           }




        if( $idVideo != '' ) {
            $contenedorMostrar .= '<div class="col-sm-6 mb-4"><div class="card videoCard">
                                          <a href="https://vimeo.com/'. $idVideo .'"  data-sub-html=".caption" style="position: absolute; width: 100%; height: 100%; z-index: 999" class="capaToc linkENlace"></a>
                                          <div style="padding:56.25% 0 0 0;position:absolute; width: 100%">
                                              <iframe src="https://player.vimeo.com/video/'. $idVideo .'?controls=0?background=1&loop=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                          </div>
                                          <img src="/wp-content/themes/santacc/dist/images/playFnd.png" class="card-img-top playImg">';
              if($txtGaleria != '' || $descGaleria != '') {
                $contenedorMostrar .='<div class="card-body caption"><h5 class="card-title">'. $txtGaleria .'</h5><p class="card-text">' .$descGaleria. '</p></div>';
              }
            $contenedorMostrar .='</a></div></div>';
              $k++;
        } else if( $contenedorMultiples != '' ) {
            $contenedorMostrar .= '<div class="col-sm-6 mb-4"><div class="card"><div class="icoMasFotos" style="position: absolute; top: 2%; right: 2%; z-index: 999"><img src="/wp-content/themes/santacc/dist/images/pilafotos.png" width="100%"></div>'.$contenedorMultiples;
            if($txtGaleria != '' || $descGaleria != '') {
              $contenedorMostrar .='<div class="card-body caption"><h5 class="card-title">'. $txtGaleria .'</h5><p class="card-text">' .$descGaleria. '</p></div>';
             }
              $contenedorMostrar .='</div></div>';
              $k++;
        } else {
          $contenedorMostrar .= '<div class="col-sm-6 mb-4"><div class="card"><a href="'. $imagenGaleria["url"] .'" data-sub-html=".caption" class="linkENlace">
                                 <img src="'. $imagenGaleria["url"] .'" class="card-img-top" alt="'. $imagenGaleria["name"] .'">';
          if($txtGaleria != '' || $descGaleria != '') {
            $contenedorMostrar .='<div class="card-body caption"><h5 class="card-title">'. $txtGaleria .'</h5><p class="card-text">' .$descGaleria. '</p></div>';
          }
                $contenedorMostrar .='</a></div></div>';
                $k++;
            }

        if($selectorClipping == 1 ) {
            if( $k == 1) {
              $contenedorMostrar .= '<div class="col-sm-6 mb-4"><div class="card"><div class="contenedorClippin">'. $contenedorClipping .'</div></div></div>';
              $k++;
            }
          }

        endwhile;
    else :
    endif;
        $contenedorMostrar .= '</div>';

  ?>





    <div class="container">
                                    <div class="row" data-masonry='{"percentPosition": true }' id="relative-caption">
                                    <?php
                                        echo $contenedorMostrar;
                                      ?>
                                    </div></div>




<script type="text/javascript">
  function downloadJSAtOnload() {
    var element = document.createElement('script');
    element.src = 'https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js';
    document.body.appendChild(element);
  }
  if (window.addEventListener)
    window.addEventListener('load', downloadJSAtOnload, false);
  else if (window.attachEvent)
    window.attachEvent('onload', downloadJSAtOnload);
  else window.onload = downloadJSAtOnload;

</script>
<?php
$varMia = '';
global $post;

$listadoTaxonomis = '';
$terms = get_the_terms( $post->ID,'tipotrabajo', '', ' ');
if($terms != '') {
  foreach ( $terms as $term ) {
    $listadoTaxonomis  = $term->slug;
  }

}

if( have_rows('colorTrabajo', 'option') ):
  while( have_rows('colorTrabajo', 'option') ) : the_row();
    $sub_tipo = get_sub_field('tipoTrabajo');
    $sub_color = get_sub_field('colorTrabajo');

    if($sub_tipo->slug === $listadoTaxonomis) {
      $varMia = $sub_color;
    }
  endwhile;
else :
endif;
?>
<style>
  .banner,  footer{
    background-color: <?php echo $varMia; ?>;
  }
  .banner nav ul li{
    border-bottom: 2px solid <?php echo $varMia; ?>;
  }
</style>
