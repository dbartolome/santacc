<footer class="content-info">
  <div class="container">
    @php dynamic_sidebar('sidebar-footer') @endphp

    <p style="text-align: center;">© 2021 - <a class="aviso_legal" href="/aviso-legal/">Aviso legal</a> - Empresa adherida a <a href="http://www.autocontrol.es/" rel="noopener" target="_blank">Autocontrol</a> - Productora asociada: <a href="https://patitopro.es/" rel="noopener" target="_blank">Patito</a></p>
  </div>
</footer>
