<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

acf_add_options_page([
    'page_title' => get_bloginfo('name') . ' theme options',
    'menu_title' => 'Opciones para el tema',
    'menu_slug'  => 'theme-options',
    'capability' => 'edit_theme_options',
    'position'   => '999',
    'autoload'   => true
]);

$options = new FieldsBuilder('theme_options');

$options
    ->setLocation('options_page', '==', 'theme-options');

$options
    ->addTab('Header', ['placement' => 'left'])
        ->addImage('logoHeader', [
            'label' => 'Logo para el header',
        ])
    ->addTab('Imagenes Agencia', ['placement' => 'left'])
    ->addImage('imagenFondo', [
        'label' => 'Imagen para el fondo de agencia',
    ])
    ->addImage('imagenSuperior', [
        'label' => 'Imagen para la parte superior de agencia',
    ])
    ->addTab('Activar filtros home', ['placement' => 'left'])
        ->addTrueFalse('filtrosHome', [
            'label' => 'Activar filtros home Si / No',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'message' => '',
            'default_value' => 0,
            'ui' => 1,
            'ui_on_text' => 'Avtivado',
            'ui_off_text' => 'Desactivado',
        ])
        ->addTab('Servicios SantaCC', ['placement' => 'left'])
            ->addRepeater('servicios', [
                'label' => 'Servicios de Santa',
                'layout' => 'row',
            ])
            ->addText('codigoIconosServicio', [
                'label' => 'Código para el icono de servicios',
            ])
            ->addNumber('tamanoIcono', [
        'label' => 'Código para el icono de servicios',
    ])

    ->addImage('iconoServicios', [
                'label' => 'Logo para el header',
                ])
            ->addText('tituloServicio', [
                'label' => 'Titulo para el servicios',
            ])
            ->addTextarea('nombreServicio', [
                'label' => 'Texto para el servicios',
            ])
        ->endRepeater();

return $options;


