<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$trabajo = new FieldsBuilder('campos_trabajo');

$trabajo
    ->setLocation('post_type', '==', 'trabajo');

$trabajo
    ->addTab('Informacion del trabajo', ['placement' => 'left'])
        ->addText('campania', [
            'label' => 'Campaña a al que pertenece',
        ])
        ->addTextarea('descripcion', [
            'label' => 'Descripcion del trabajo',
        ])
        ->addImage('logoCliente', [
            'label' => 'Logo del cliente de la campaña',
        ])

        ->addImage('fondoAnimado', [
            'label' => 'Gif para el fondo del grid roll-over',
        ])

        ->addImage('imagenHome', [
            'label' => 'Imagen para la home',
        ])

    ->addTab('Imágenes de la galería', ['placement' => 'left'])
        ->addRepeater('galeriaImagenes', [
            'label' => 'Agregar imagenes',
            'layout' => 'table',
        ])
            ->addImage('imageGaleria', [
                'label' => 'Imagen para la galería',
            ])
        ->addText('idVideo', [
            'label' => 'Agregar aqui solo el Id del video ',
        ])
            ->addText('txtImagenes', [
                'label' => 'Texto para las imagenes',
            ])
            ->addText('descImagenes', [
                'label' => 'descripción para las imagenes',
            ])
            ->addGallery('grupoImagenes', [
                'label' => 'Grupo imágenes para un trabajo',
                'instructions' => '',
                'insert' => 'end',
            ])
        ->endRepeater()
    ->addTab('Destacados', ['placement' => 'left'])
        ->addRepeater('trDestacados', [
            'label' => 'Agregar imagenes',
            'layout' => 'table',
        ])
            ->addImage('imageDestacada', [
                'label' => 'Imagen para la galería',
            ])
            ->addText('idVideoDestacado', [
                'label' => 'Introducir el id para el video destacado',
            ])
        ->endRepeater()
    ->addTab('Info Clipping', ['placement' => 'left'])
    ->addTrueFalse('tieneClipping', [
        'label' => 'Si / No Clipping',
        'instructions' => '',
        'required' => 0,
        'default_value' => 0,
        'ui' => 1,
        'ui_on_text' => 'Si',
        'ui_off_text' => 'No',
    ])
        ->addRepeater('clipping', [
            'label' => 'Noticias para el clipping',
            'layout' => 'table',
            'min' => 0,
            'max' => 10,
        ])
            ->addText('nombreMedio', [
                'label' => 'Texto para las imagenes',
            ])
            ->addText('titularNoticia', [
                'label' => 'Texto para las Noticias',
            ])
            ->addUrl('urlNoticia', [
                'label' => 'Url para la noticia del clipping',
            ])
        ->endRepeater()
;

return $trabajo;
