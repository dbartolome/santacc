<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$equipo = new FieldsBuilder('campos_equipo');

$equipo
    ->setLocation('post_type', '==', 'equipo');

$equipo
    ->addTab('Datos personales', ['placement' => 'left'])
        ->addText('puesto', [
            'label' => 'Puesto que desempeña en SantaCC',
        ])
        ->addImage('imagenFiltro', [
            'label' => 'Foto con el filtro amarillo par el roll over',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'return_format' => 'array',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])

;

return $equipo;

